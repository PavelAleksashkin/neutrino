$(document).ready(function() {
    $(".owl-carousel").owlCarousel({
        items: 1,
        nav:true,
        navText: ['',''],
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        loop: true,
        responsive:{ // уменьшаем кол-во слайдов
      	0:{ // ширина от 0 px
        	nav:false
        },
      	767:{ // от 600 px
        },
      	1000:{ // от 1000 px
        }
    }
    });
    $('input, select').styler();
    $(".header .ico_mobile_menu").click(function() {
        $(this).toggleClass("active");
        $(this).next().slideToggle();
    });
});
